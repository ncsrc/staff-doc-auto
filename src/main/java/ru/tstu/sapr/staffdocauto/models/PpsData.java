package ru.tstu.sapr.staffdocauto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PpsData {

	private long ppsId;
	private String teacherName;
	private String employmentType;
	private String academicRank;
	private String subjects;
	private String education;
	private String additionalEducation;


	public PpsData(String teacherName, String employmentType, String academicRank, String subjects, String education, String additionalEducation) {
		this.teacherName = teacherName;
		this.employmentType = employmentType;
		this.academicRank = academicRank;
		this.subjects = subjects;
		this.education = education;
		this.additionalEducation = additionalEducation;
	}


}
