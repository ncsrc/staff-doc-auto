package ru.tstu.sapr.staffdocauto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


// TODO rename to row ?
@Data
@AllArgsConstructor
@NoArgsConstructor
public class XlsFileData {

	private long xlsFileId;
	private String subject;
	private String teacherLastName;
	private double teacherShare;


	public XlsFileData(final String subject, final String teacherLastName, final double teacherShare) {
		this.subject = subject;
		this.teacherLastName = teacherLastName;
		this.teacherShare = teacherShare;
	}


}
