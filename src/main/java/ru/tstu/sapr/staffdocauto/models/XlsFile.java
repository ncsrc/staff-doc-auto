package ru.tstu.sapr.staffdocauto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class XlsFile {

	private long id;
	private int studentSetId;


}
