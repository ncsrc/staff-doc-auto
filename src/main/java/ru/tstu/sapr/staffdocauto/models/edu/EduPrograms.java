package ru.tstu.sapr.staffdocauto.models.edu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EduPrograms {

	private long id;
	private String directionCode;
	private String programName;


}
