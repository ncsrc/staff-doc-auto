package ru.tstu.sapr.staffdocauto.models.edu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EduDirections {

	private String code;
	private String directionName;
	private EduQualificationType eduQualificationType;



}
