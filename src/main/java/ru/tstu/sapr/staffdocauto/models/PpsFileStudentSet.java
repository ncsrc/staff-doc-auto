package ru.tstu.sapr.staffdocauto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Many-to-many between pps files and student sets
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PpsFileStudentSet {

    private long studentSetId;
    private long ppsId;


}
