package ru.tstu.sapr.staffdocauto.models.edu;


public enum EduScheduleType {

	FULLTIME("очная"),
	EXTRAMURAL("заочная");

	public final String type;

	EduScheduleType(String type) {
		this.type = type;
	}

}
