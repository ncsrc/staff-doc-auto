package ru.tstu.sapr.staffdocauto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentSet {

	private long id;
	private long eduProgramId;
	private EduScheduleType eduScheduleType;
	private int year;


	public StudentSet(long eduProgramId, EduScheduleType eduScheduleType, int year) {
		this.eduProgramId = eduProgramId;
		this.eduScheduleType = eduScheduleType;
		this.year = year;
	}
}
