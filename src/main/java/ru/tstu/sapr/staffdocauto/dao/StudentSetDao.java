package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.tstu.sapr.staffdocauto.models.StudentSet;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;
import ru.tstu.sapr.staffdocauto.ui.models.Task;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRow;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRequest;

import java.sql.PreparedStatement;
import java.util.List;


@Repository
@DependsOn("h2")
public class StudentSetDao {

    private static final String SQL_GET_DATAGRID_MODEL = "SELECT s.id, s.year, s.edu_schedule_type,\n" +
            "ep.program_name, ed.direction_name, ed.edu_qualification_type\n" +
            "FROM student_sets s\n" +
            "JOIN edu_programs ep ON s.edu_program_id = ep.id\n" +
            "JOIN edu_directions ed ON ep.direction_code = ed.code\n" +
            "ORDER BY %s %s\n" +
            "LIMIT :limit\n" +
            "OFFSET :offset";

    private static final String SQL_COUNT_ALL = "SELECT COUNT(*) FROM student_sets";

    private static final String SQL_GET_EDU_DATA_BY_STUDENT_ID = "SELECT s.year, s.edu_schedule_type,\n" +
            "ep.program_name, ep.direction_code,\n" +
            "ed.direction_name\n" +
            "FROM student_sets s\n" +
            "JOIN edu_programs ep ON s.edu_program_id = ep.id\n" +
            "JOIN edu_directions ed ON ep.direction_code = ed.code\n" +
            "WHERE s.id = :student_set_id";

    private static final String SQL_DELETE_BY_ID = "DELETE FROM student_sets\n" +
            "WHERE id=:id";


    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final SimpleJdbcInsert jdbcInsert;


    @Autowired
    public StudentSetDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        this.jdbcInsert.withTableName("student_sets")
                .usingGeneratedKeyColumns("id")
                .usingColumns("year", "edu_schedule_type", "edu_program_id");
    }


    public List<DatagridRow> getStudentSetsForUi(final DatagridRequest datagridRequest) {

        return jdbcTemplate.query(
                getQueryWithOrderByClause(datagridRequest),

                new MapSqlParameterSource()
                        .addValue("limit", datagridRequest.getRows())
                        .addValue("offset", calculateOffset(datagridRequest)),

                new BeanPropertyRowMapper<>(DatagridRow.class)
        );
    }


    public int countAll() {
        return jdbcTemplate.queryForObject(
                SQL_COUNT_ALL,
                new MapSqlParameterSource(),
                Integer.class
        );
    }


    public EducationData getEducationDataByStudentSetId(final int id) {

        return jdbcTemplate.queryForObject(
                SQL_GET_EDU_DATA_BY_STUDENT_ID,
                new MapSqlParameterSource().addValue("student_set_id", id),
                new BeanPropertyRowMapper<>(EducationData.class)
        );

    }


    public int insert(final Task studentSet) {
        return jdbcInsert.executeAndReturnKey(
                new MapSqlParameterSource()
                        .addValue("year", studentSet.getYear())
                        .addValue("edu_schedule_type", studentSet.getEduScheduleType().name())
                        .addValue("edu_program_id", studentSet.getEduProgramId())
        ).intValue();
    }


    public void deleteById(final int studentSetId) {
        jdbcTemplate.update(
                SQL_DELETE_BY_ID,
                new MapSqlParameterSource().addValue("id", studentSetId)
        );
    }


    private String getQueryWithOrderByClause(final DatagridRequest request) {
        return String.format(SQL_GET_DATAGRID_MODEL, request.getSort(), request.getOrder());
    }


    private int calculateOffset(final DatagridRequest request) {
        return (request.getPage() - 1) * request.getRows();
    }

}
