package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.tstu.sapr.staffdocauto.models.PpsFile;

import java.util.List;


@Repository
@DependsOn("h2")
public class PpsFileDao {

    static final String SQL_GET_FILENAME_BY_STUDENT_SET_ID = "SELECT pf.filename FROM pps_files pf " +
            "JOIN pps_files_student_sets pfs ON pf.id = pfs.pps_id " +
            "WHERE pfs.student_set_id = :student_set_id";

    static final String SQL_INSERT_PPS_STUDENT_SET_RELATION = "INSERT INTO pps_files_student_sets VALUES (:student_set_id, :pps_id)";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final SimpleJdbcInsert jdbcInsert;


    @Autowired
    public PpsFileDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        this.jdbcInsert.withTableName("pps_files")
                .usingGeneratedKeyColumns("id")
                .usingColumns("filename");
    }


    public int insertFile(final String filename) {
        return jdbcInsert.executeAndReturnKey(
                new MapSqlParameterSource()
                        .addValue("filename", filename)
        ).intValue();
    }


    public void insertFileStudentSetRelation(final int studentSetId, final int ppsId) {
        jdbcTemplate.update(
                SQL_INSERT_PPS_STUDENT_SET_RELATION,
                new MapSqlParameterSource()
                        .addValue("student_set_id", studentSetId)
                        .addValue("pps_id", ppsId)
        );
    }


    public String getFilenameByStudentSetId(final int studentSetId) {
        return jdbcTemplate.queryForObject(
                SQL_GET_FILENAME_BY_STUDENT_SET_ID,
                new MapSqlParameterSource().addValue("student_set_id", studentSetId),
                String.class
        );
    }


    public List<PpsFile> getAllFiles() {
        return jdbcTemplate.query(
                "SELECT * FROM pps_files",
                new MapSqlParameterSource(),
                new BeanPropertyRowMapper<>(PpsFile.class)
        );
    }

}
