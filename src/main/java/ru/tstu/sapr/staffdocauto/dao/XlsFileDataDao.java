package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;

import java.util.List;


@Repository
@DependsOn("h2")
public class XlsFileDataDao {

	static final String SQL_INSERT = "INSERT INTO xls_files_data (xls_file_id, subject, teacher_last_name, teacher_share) " +
			"VALUES (:xls_file_id, :subject, :teacher_last_name, :teacher_share)";

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public XlsFileDataDao(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public void insert(final List<XlsFileData> fileData) {

		jdbcTemplate.batchUpdate(
				SQL_INSERT,

				fileData.stream()
						.map(file -> new MapSqlParameterSource()
								.addValue("xls_file_id", file.getXlsFileId())
								.addValue("subject", file.getSubject())
								.addValue("teacher_last_name", file.getTeacherLastName())
								.addValue("teacher_share", file.getTeacherShare())
						).toArray(MapSqlParameterSource[]::new)
		);

	}


}
