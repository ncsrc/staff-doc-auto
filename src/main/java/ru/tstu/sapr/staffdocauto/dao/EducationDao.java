package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.tstu.sapr.staffdocauto.models.edu.EduDirections;
import ru.tstu.sapr.staffdocauto.models.edu.EduPrograms;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;

import java.util.List;


@Repository
@DependsOn("h2")
public class EducationDao {

    static final String SQL_GET_EDU_DIRECTIONS = "SELECT * FROM edu_directions " +
            "WHERE edu_qualification_type = :edu_qualification_type";

    static final String SQL_GET_PROGRAMS = "SELECT * FROM edu_programs\n" +
			"WHERE direction_code = :direction_code";


    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;


    public List<EduDirections> getEduDirectionsByQualificationType(final EduQualificationType qualification) {
        return jdbcTemplate.query(
                SQL_GET_EDU_DIRECTIONS,
                new MapSqlParameterSource().addValue("edu_qualification_type", qualification.name()),
                new BeanPropertyRowMapper<>(EduDirections.class)
        );
    }


    public List<EduPrograms> getEduProgramsByDirectionCode(final String directionCode) {
        return jdbcTemplate.query(
                SQL_GET_PROGRAMS,
                new MapSqlParameterSource().addValue("direction_code", directionCode),
                new BeanPropertyRowMapper<>(EduPrograms.class)
        );
    }

}
