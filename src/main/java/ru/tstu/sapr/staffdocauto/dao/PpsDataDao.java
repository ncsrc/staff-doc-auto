package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.models.Teacher;

import java.util.List;


@Repository
@DependsOn("h2")
public class PpsDataDao {

	static final String SQL_INSERT = "INSERT INTO pps_files_data \n" +
			"(pps_id, teacher_name, employment_type, academic_rank, subjects, education, additional_education)\n" +
			"VALUES (:pps_id, :teacher_name, :employment_type, :academic_rank, :subjects, :education, :additional_education)";

	static final String SQL_GET_TEACHERS_OF_STUDENT_SET = "SELECT ss.id, pfd.teacher_name, pfd.employment_type, pfd.academic_rank,\n" +
			"xfd.subject, pfd.education, pfd.additional_education, xfd.teacher_share\n" +
			"FROM student_sets ss\n" +
			"JOIN pps_files_student_sets pf ON ss.id = pf.student_set_id\n" +
			"JOIN pps_files_data pfd ON pf.pps_id = pfd.pps_id\n" +
			"JOIN xls_files xf ON ss.id = xf.student_set_id\n" +
			"JOIN xls_files_data xfd ON xf.id = xfd.xls_file_id\n" +
			"WHERE ss.id = :student_set_id\n" +
			"AND pfd.teacher_name ILIKE ('%' || xfd.teacher_last_name || '%')\n" +
			"AND pfd.subjects ILIKE ('%' || xfd.subject || '%')";

	private final NamedParameterJdbcTemplate jdbcTemplate;


	@Autowired
	public PpsDataDao(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public void insert(final List<PpsData> ppsData) {

		jdbcTemplate.batchUpdate(
				SQL_INSERT,

				ppsData.stream()
					   .map(ppsRow -> new MapSqlParameterSource()
							.addValue("pps_id", ppsRow.getPpsId())
							.addValue("teacher_name", ppsRow.getTeacherName())
							.addValue("employment_type", ppsRow.getEmploymentType())
							.addValue("academic_rank", ppsRow.getAcademicRank())
							.addValue("subjects", ppsRow.getSubjects())
							.addValue("education", ppsRow.getEducation())
							.addValue("additional_education", ppsRow.getAdditionalEducation()))
					   .toArray(MapSqlParameterSource[]::new)
		);

	}


	/**
	 * Returns matches between XLS and PPS data of specific student set.
	 * @param studentSetId
	 * @return
	 */
	public List<Teacher> getTeachersByStudentSetId(final int studentSetId) {

		return jdbcTemplate.query(
				SQL_GET_TEACHERS_OF_STUDENT_SET,
				new MapSqlParameterSource().addValue("student_set_id", studentSetId),
				new BeanPropertyRowMapper<>(Teacher.class)
		);

	}


}
