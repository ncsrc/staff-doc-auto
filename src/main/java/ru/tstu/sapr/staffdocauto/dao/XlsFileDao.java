package ru.tstu.sapr.staffdocauto.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;


@Repository
@DependsOn("h2")
public class XlsFileDao {

	private final NamedParameterJdbcTemplate jdbcTemplate;

	private final SimpleJdbcInsert jdbcInsert;


	@Autowired
	public XlsFileDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = namedParameterJdbcTemplate;
		this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
		this.jdbcInsert.withTableName("xls_files")
				.usingGeneratedKeyColumns("id")
				.usingColumns("student_set_id");
	}


	public int insert(final int studentSetId) {
		return jdbcInsert.executeAndReturnKey(
				new MapSqlParameterSource().addValue("student_set_id", studentSetId)
		).intValue();
	}

}
