package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions;


public class XlsParserException extends Exception {

    public XlsParserException() {
        super();
    }

    public XlsParserException(final String message) {
        super(message);
    }

    public XlsParserException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
