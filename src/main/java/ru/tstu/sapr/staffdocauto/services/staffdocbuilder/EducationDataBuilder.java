package ru.tstu.sapr.staffdocauto.services.staffdocbuilder;

import org.apache.poi.xwpf.usermodel.*;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.InsufficientDocDataException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;


class EducationDataBuilder {


    // template values in paragraphs for replacement
    private static final String TEMPLATE_EDU_PROGRAM = "edu_program";
    private static final String TEMPLATE_EDU_DIRECTION = "edu_direction";
    private static final String TEMPLATE_EDU_SET = "edu_set";

    private EducationData educationData;

    private XWPFDocument template;


    EducationDataBuilder(final XWPFDocument template) {
        this.template = template;
    }


    public void setEducationData(final EducationData educationData) {
        this.educationData = educationData;
    }


    public void build() throws InsufficientDocDataException {

        if (educationData == null) {
            throw new InsufficientDocDataException("Education data is required for building document.");
        }

        writeTables();
        writeParagraphs();
    }


    private void writeTables() {
        mainLoop:
        for (XWPFTable table : template.getTables()) {

            for (XWPFTableRow row : table.getRows()) {

                for (XWPFTableCell cell : row.getTableCells()) {

                    for (XWPFParagraph paragraph : cell.getParagraphs()) {

                        for (XWPFRun run : paragraph.getRuns()) {
                            String text = run.getText(0);

                            if (contains(text, TEMPLATE_EDU_PROGRAM)) {
                                replaceText(run, text, TEMPLATE_EDU_PROGRAM, educationData.getProgramName());
                            }

                            if (contains(text, TEMPLATE_EDU_DIRECTION)) {
                                replaceText(run, text, TEMPLATE_EDU_DIRECTION, getEduDirection());
                                break mainLoop;
                            }
                        }

                    }

                }
            }

        }
    }


    private void writeParagraphs() {
        mainLoop:
        for (final XWPFParagraph paragraph : template.getParagraphs()) {

            for (final XWPFRun run : paragraph.getRuns()) {
                String text = run.getText(0);

                if (contains(text, TEMPLATE_EDU_SET)) {
                    replaceText(run, text, TEMPLATE_EDU_SET, getEduSet());
                    break mainLoop;
                }

            }

        }
    }


    private String getEduSet() {
        return educationData.getYear() + " год набора, " + educationData.getEduScheduleType().type + " форма обучения";
    }


    private String getEduDirection() {
        return educationData.getDirectionCode() + " " + educationData.getDirectionName();
    }


    private void replaceText(final XWPFRun run, final String text, final String templateValue, final String data) {
        final String replacedText = text.replace(templateValue, data);
        run.setText(replacedText, 0);
    }


    private boolean contains(final String text, final String templateValue) {
        return text != null && text.contains(templateValue);
    }


}
