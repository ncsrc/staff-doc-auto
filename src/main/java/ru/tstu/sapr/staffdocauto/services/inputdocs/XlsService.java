package ru.tstu.sapr.staffdocauto.services.inputdocs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.dao.XlsFileDao;
import ru.tstu.sapr.staffdocauto.dao.XlsFileDataDao;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.XlsParser;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;

import java.util.List;


@Service
@Slf4j
public class XlsService {

    @Autowired
    private XlsFileDataDao xlsDataDao;

    @Autowired
    private XlsFileDao xlsFileDao;

    @Autowired
    private XlsParser xlsParser;


    public List<XlsFileData> parseXlsFile(final MultipartFile ppsFile) throws XlsParserException {
        log.info("Starting to parse XLS file");
        return xlsParser.parse(ppsFile);
    }


    public int saveFile(final int studentSetId) {
        return xlsFileDao.insert(studentSetId);
    }


    public void saveXlsData(final List<XlsFileData> xlsData, final int xlsId) {
        log.info("Saving parsed XLS data for XLS file #{}", xlsId);
        xlsData.forEach(xlsDataRow -> xlsDataRow.setXlsFileId(xlsId));  // set xls file reference for each row
        xlsDataDao.insert(xlsData);
    }



}
