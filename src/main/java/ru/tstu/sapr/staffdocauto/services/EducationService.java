package ru.tstu.sapr.staffdocauto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tstu.sapr.staffdocauto.dao.EducationDao;
import ru.tstu.sapr.staffdocauto.models.edu.EduDirections;
import ru.tstu.sapr.staffdocauto.models.edu.EduPrograms;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;

import java.util.List;


@Service
public class EducationService {


    @Autowired
    EducationDao educationDao;


    public List<EduDirections> getEduDirections(final EduQualificationType qualification) {
        return educationDao.getEduDirectionsByQualificationType(qualification);
    }


    public List<EduPrograms> getEduPrograms(final String directionCode) {
        return educationDao.getEduProgramsByDirectionCode(directionCode);
    }


}
