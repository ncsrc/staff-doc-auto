package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.OLE2NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.IncorrectXlsFileException;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;


@Service
@Slf4j
public class XlsParser {

	private static final String KEY_SUBJECT = "subject";
	private static final String KEY_TEACHER = "teacher";
	private static final String KEY_SHARE = "share";


	public List<XlsFileData> parse(final MultipartFile file) throws XlsParserException {

		try (final XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream())) {

			final XSSFSheet sheet = workbook.getSheetAt(0);
			final int startRowNumber = findStartRow(sheet);
			final Map<String, Integer> columnsIndexes = getColumnsIndexes(sheet, startRowNumber);

			return getXlsFileData(sheet, startRowNumber, columnsIndexes);

		} catch (OLE2NotOfficeXmlFileException e) {
			log.error("Got incorrect XLS file, should be XLSX");
			throw new IncorrectXlsFileException("Некорректный формат файла, файл должен быть формата xlsx.", e);
		} catch (IOException e) {
			log.error("Error while reading XLS file occurred");
			throw new XlsParserException("Ошибка чтения файла", e);
		}

	}


	private List<XlsFileData> getXlsFileData(final XSSFSheet sheet, int startRowNumber, final Map<String, Integer> columnsIndexes) throws XlsParserException {

		try {

			final List<XlsFileData> xlsFileData = new ArrayList<>();

			while (true) {
				final XSSFRow row = sheet.getRow(++startRowNumber);
				final XSSFCell subjectCell = row.getCell(columnsIndexes.get(KEY_SUBJECT), Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);

				// TODO: impl VCR parsing later
				if (subjectCell == null || subjectCell.getStringCellValue().contains("ВКР")) {
					break;
				}

				xlsFileData.add(
						new XlsFileData(
								subjectCell.getStringCellValue().trim(),
								row.getCell(columnsIndexes.get(KEY_TEACHER), Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue().trim(),
								getShareValue(row, columnsIndexes.get(KEY_SHARE))
						)
				);

			}

			return xlsFileData;

		} catch (Exception e) {

			throw new XlsParserException("Some error during exceptions parsing has occurred ", e);

		}

	}


	// rounds share value to e.g. 0.05
	private double getShareValue(final XSSFRow row, final int columnIndex) {
		final XSSFCell shareCell = row.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

		final DecimalFormat decimalFormat = new DecimalFormat("#.##");
		decimalFormat.setRoundingMode(RoundingMode.HALF_EVEN);

		return Double.valueOf(decimalFormat.format(shareCell.getNumericCellValue()));
	}


	// Get map with cell values where content resides
	private Map<String, Integer> getColumnsIndexes(final XSSFSheet sheet, final int startRowNumber) throws IncorrectXlsFileException {
		final Map<String, Integer> columns = new HashMap<>();
		columns.put(KEY_SUBJECT, 0);

		final XSSFRow startRow = sheet.getRow(startRowNumber);
		final Iterator<Cell> cellIterator = startRow.cellIterator();

		while (cellIterator.hasNext()) {
			final Cell cell = cellIterator.next();
			final String cellValue = cell.getStringCellValue().toLowerCase();

			if (cellValue.contains("преподаватель")) {
				columns.put(KEY_TEACHER, cell.getColumnIndex());
			} else if (cellValue.contains("доля")) {
				columns.put(KEY_SHARE, cell.getColumnIndex());
			}
		}

		checkIfColumnsFound(columns);

		return columns;
	}


	private void checkIfColumnsFound(final Map<String, Integer> columns) throws IncorrectXlsFileException {
		if (!columns.containsKey(KEY_TEACHER)) {
			throw new IncorrectXlsFileException("Колонка 'преподаватель' не найдена");
		}

		if (!columns.containsKey(KEY_SHARE)) {
			throw new IncorrectXlsFileException("Колонка 'доля' не найдена");
		}
	}


	private int findStartRow(final XSSFSheet sheet) throws IncorrectXlsFileException {
		final Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			final Row row = rowIterator.next();
			final Cell cell = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

			final String cellValue = cell.getStringCellValue().toLowerCase();

			if (cellValue.contains("дисциплина")) {
				return row.getRowNum();
			}
		}

		throw new IncorrectXlsFileException("Колонка 'Дисциплина' не найдена");
	}


}
