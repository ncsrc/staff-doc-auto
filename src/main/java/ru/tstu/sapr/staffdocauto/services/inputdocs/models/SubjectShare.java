package ru.tstu.sapr.staffdocauto.services.inputdocs.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubjectShare {

	private String subject;
	private double share;


}
