package ru.tstu.sapr.staffdocauto.services.inputdocs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.dao.PpsDataDao;
import ru.tstu.sapr.staffdocauto.dao.PpsFileDao;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.models.PpsFile;
import ru.tstu.sapr.staffdocauto.services.inputdocs.models.SubjectShare;
import ru.tstu.sapr.staffdocauto.services.inputdocs.models.Teacher;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.PpsParser;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class PpsService {


	@Autowired
	PpsFileDao ppsFileDao;

	@Autowired
	PpsDataDao ppsDataDao;

	@Autowired
	PpsParser ppsParser;


	public List<PpsData> parsePpsFile(final MultipartFile ppsFile) throws XlsParserException {
		log.info("Starting to parse PPS file");
		return ppsParser.parse(ppsFile);
	}


	public void savePpsFileContent(final List<PpsData> ppsData, final int ppsId) {
		log.info("Saving PPS file data for PPS file #{}", ppsId);
		ppsData.forEach(ppsRow -> ppsRow.setPpsId(ppsId));	// set pps id for each row
		ppsDataDao.insert(ppsData);
	}


	public int saveFile(final int studentSetId, final String filename) {
		final int ppsId = ppsFileDao.insertFile(filename);
		ppsFileDao.insertFileStudentSetRelation(studentSetId, ppsId);

		return ppsId;
	}


	public void linkStudentSetWithExistingPps(final int ppsId, final int studentSetId) {
		ppsFileDao.insertFileStudentSetRelation(studentSetId, ppsId);
	}


	public List<PpsFile> getFiles() {
		return ppsFileDao.getAllFiles();
	}


	public List<TeacherRow> getTeachersDataForDocBuilder(final int studentSetId) {
		log.info("Starting getting data for document builder");
		final List<Teacher> teachersWithSubjectsAndShares = ppsDataDao.getTeachersByStudentSetId(studentSetId);
		final Map<String, Teacher> teachers = groupTeachersByTheirName(teachersWithSubjectsAndShares);
		final Map<String, List<SubjectShare>> teacherSubjectShares = groupSubjectsAndSharesByTeachers(teachersWithSubjectsAndShares);

		return getTeacherRows(teachers, teacherSubjectShares);
	}


	public String getFilename(final int studentSetId) {
		return ppsFileDao.getFilenameByStudentSetId(studentSetId);
	}


	private List<TeacherRow> getTeacherRows(final Map<String, Teacher> teachers, final Map<String, List<SubjectShare>> teacherSubjectShares) {
		int idCounter = 1;
		final List<TeacherRow> teacherRows = new ArrayList<>();

		for (Map.Entry<String, List<SubjectShare>> teacher : teacherSubjectShares.entrySet()) {
			final String teacherName = teacher.getKey();
			teacherRows.add(new TeacherRow(
					String.valueOf(idCounter++), teacherName,
					teachers.get(teacherName).getEmploymentType(),
					teachers.get(teacherName).getAcademicRank(),
					collectSubjects(teacher),
					teachers.get(teacherName).getEducation(),
					teachers.get(teacherName).getAdditionalEducation(),
					collectShares(teacher)
			));

		}
		return teacherRows;
	}


	private List<Double> collectShares(Map.Entry<String, List<SubjectShare>> teacher) {
		return teacher.getValue().stream()
				.map(SubjectShare::getShare)
				.collect(Collectors.toList());
	}

	private List<String> collectSubjects(Map.Entry<String, List<SubjectShare>> teacher) {
		return teacher.getValue().stream()
				.map(SubjectShare::getSubject)
				.collect(Collectors.toList());
	}


	private Map<String, Teacher> groupTeachersByTheirName(List<Teacher> teachersWithSubjectsAndShares) {
		return teachersWithSubjectsAndShares.stream()
				.distinct()
				.collect(Collectors.toMap(
						Teacher::getTeacherName,
						teacher -> teacher,
						(teacher, teacher2) -> teacher
				));
	}


	private Map<String, List<SubjectShare>> groupSubjectsAndSharesByTeachers(final List<Teacher> teachers) {
		return teachers.stream()
				.sorted()
				.collect(
						Collectors.groupingBy(
								Teacher::getTeacherName,
								Collectors.mapping(teacher -> new SubjectShare(
												teacher.getSubject(), teacher.getTeacherShare()),
										Collectors.toList()
								)
						)
				);
	}


}
