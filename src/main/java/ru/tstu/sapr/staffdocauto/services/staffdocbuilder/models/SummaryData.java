package ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SummaryData {

    private int totalTeachers;
    private double totalShares;
    private String ppsFullYear;
    private String ppsYears;


}
