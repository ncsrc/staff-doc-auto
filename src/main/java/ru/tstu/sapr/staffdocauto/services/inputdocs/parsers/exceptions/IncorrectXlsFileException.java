package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions;


public class IncorrectXlsFileException extends XlsParserException {

    public IncorrectXlsFileException() {
        super();
    }

    public IncorrectXlsFileException(final String message) {
        super(message);
    }

    public IncorrectXlsFileException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
