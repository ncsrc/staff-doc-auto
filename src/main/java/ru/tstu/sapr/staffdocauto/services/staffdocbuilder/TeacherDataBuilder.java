package ru.tstu.sapr.staffdocauto.services.staffdocbuilder;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.InsufficientDocDataException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


class TeacherDataBuilder {

    // cell numbers in teachers table
    private static final int TEACHER_CELL_ID = 0;
    private static final int TEACHER_CELL_NAME = 1;
    private static final int TEACHER_CELL_EMPLOYMENT = 2;
    private static final int TEACHER_CELL_RANK = 3;
    private static final int TEACHER_CELL_SUBJECTS = 4;
    private static final int TEACHER_CELL_EDUCATION = 5;
    private static final int TEACHER_CELL_ADDITIONAL_EDUCATION = 6;
    private static final int TEACHER_CELL_SHARES = 7;

    private List<TeacherRow> teachers;

    private XWPFDocument template;


    TeacherDataBuilder(final XWPFDocument template) {
        this.template = template;
    }


    public void setTeachersData(final List<TeacherRow> teachers) {
        this.teachers = teachers;
    }


    public void build() throws InsufficientDocDataException {

        if (teachers == null) {
            throw new InsufficientDocDataException("Teachers data is required for building document.");
        }

        teachers.forEach((teacher) -> {
            final XWPFTable teacherTable = prepareTable();
            final int startRow = teacherTable.getNumberOfRows();
            final int endRow = writeTeacherData(teacher, teacherTable);
            mergeCells(teacherTable, startRow, endRow);
        });

    }


    private int writeTeacherData(final TeacherRow teacher, final XWPFTable teacherTable) {
        writeMetadata(teacherTable, teacher);
        return writeSubjectsAndShare(teacher, teacherTable);
    }


    private XWPFTable prepareTable() {
        final XWPFTable teacherTable = template.getTableArray(3); // finds teacher's table
        adjustTableLayout(teacherTable);
        return teacherTable;
    }


    // setting correct table / columns layout
    private void adjustTableLayout(XWPFTable teacherTable) {
        final CTTblLayoutType tableLayout = teacherTable.getCTTbl().getTblPr().addNewTblLayout();
        tableLayout.setType(STTblLayoutType.FIXED);
    }


    // number of iterations is determined by number of subjects and shares
    // starts from 1 because data in 0 index was already inserted with metadata
    private int writeSubjectsAndShare(final TeacherRow data, final XWPFTable teacherTable) {
        for (int i = 1; i < data.getSubjects().size(); i++) {
            final XWPFTableRow row = teacherTable.createRow();
            writeToCell(row, TEACHER_CELL_SUBJECTS, data.getSubjects().get(i));
            writeToCell(row, TEACHER_CELL_SHARES, String.valueOf(data.getShares().get(i)));
            setCellBorders(row);
        }

        return teacherTable.getNumberOfRows();
    }


    // sets teacher specific data like name, education, etc. Also sets first subject and share
    private void writeMetadata(final XWPFTable table, final TeacherRow teacher) {
        final XWPFTableRow row = table.createRow();
        final Map<Integer, String> linkedDataWithCells = linkMetadataWithCells(teacher);
        writeToCell(row, linkedDataWithCells);
        setCellBorders(row);
    }


    // links teacher data and cell numbers
    private Map<Integer, String> linkMetadataWithCells(final TeacherRow teacher) {
        Map<Integer, String> teacherData = new HashMap<>();
        teacherData.put(TEACHER_CELL_ID, teacher.getId());
        teacherData.put(TEACHER_CELL_NAME, teacher.getTeacherName());
        teacherData.put(TEACHER_CELL_EMPLOYMENT, teacher.getEmploymentType());
        teacherData.put(TEACHER_CELL_RANK, teacher.getAcademicRank());
        teacherData.put(TEACHER_CELL_SUBJECTS, teacher.getSubjects().get(0));
        teacherData.put(TEACHER_CELL_EDUCATION, teacher.getEducation());
        teacherData.put(TEACHER_CELL_ADDITIONAL_EDUCATION, teacher.getAdditionalEducation());
        teacherData.put(TEACHER_CELL_SHARES, String.valueOf(teacher.getShares().get(0)));
        return teacherData;
    }


    private void writeToCell(final XWPFTableRow row, final Map<Integer, String> data) {
        data.forEach((key, value) -> {
            final XWPFTableCell cell = row.getCell(key);
            final XWPFParagraph paragraph = cell.addParagraph();
            paragraph.createRun().setText(value);
        });
    }


    private void writeToCell(final XWPFTableRow row, final int cellNumber, final String data) {
        final XWPFTableCell cell = row.getCell(cellNumber);
        final XWPFParagraph paragraph = cell.addParagraph();
        paragraph.createRun().setText(data);
    }

    private void setCellBorders(XWPFTableRow row) {
        row.getTableCells().forEach((cell) -> {
            final CTTcBorders cellBorders = cell.getCTTc().addNewTcPr().addNewTcBorders();
            cellBorders.addNewBottom().setVal(STBorder.SINGLE);
            cellBorders.addNewTop().setVal(STBorder.SINGLE);
            cellBorders.addNewLeft().setVal(STBorder.SINGLE);
            cellBorders.addNewRight().setVal(STBorder.SINGLE);
        });
    }


    private void mergeCells(XWPFTable table, int fromRow, int toRow) {

        final XWPFTableRow firstRow = table.getRow(fromRow);

        initMerging(
                firstRow,
                TEACHER_CELL_ID, TEACHER_CELL_NAME, TEACHER_CELL_EMPLOYMENT,
                TEACHER_CELL_RANK, TEACHER_CELL_EDUCATION, TEACHER_CELL_ADDITIONAL_EDUCATION
        );

        for (int i = fromRow; i < toRow; i++) {
            final XWPFTableRow row = table.getRow(i);

            mergeCell(
                    row,
                    TEACHER_CELL_ID, TEACHER_CELL_NAME, TEACHER_CELL_EMPLOYMENT,
                    TEACHER_CELL_RANK, TEACHER_CELL_EDUCATION, TEACHER_CELL_ADDITIONAL_EDUCATION
            );

        }

    }



    // For first row
    private void initMerging(XWPFTableRow row, int... cells) {
        for (int cell : cells) {
            row.getCell(cell).getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
        }
    }


    // For remaining rows
    private void mergeCell(XWPFTableRow row, int... cells) {
        for (int cell : cells) {
            row.getCell(cell).getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
        }
    }

}
