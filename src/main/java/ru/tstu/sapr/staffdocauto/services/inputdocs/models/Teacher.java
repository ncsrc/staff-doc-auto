package ru.tstu.sapr.staffdocauto.services.inputdocs.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher implements Comparable<Teacher> {

	private String teacherName;
	private String employmentType;
	private String academicRank;
	private String subject;
	private String education;
	private String additionalEducation;
	private double teacherShare;


	@Override
	public int compareTo(final Teacher other) {
		return -this.teacherName.compareTo(other.teacherName);
	}

}
