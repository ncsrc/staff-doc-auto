package ru.tstu.sapr.staffdocauto.services.staffdocbuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Service;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.InsufficientDocDataException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.StaffDocBuilderException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.SummaryData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


@Service
@Slf4j
public class StaffDocBuilder {

	private EducationDataBuilder educationDataBuilder;
	private TeacherDataBuilder teacherDataBuilder;
	private SummaryDataBuilder summaryDataBuilder;

	XWPFDocument template;


	public StaffDocBuilder openTemplateFile() throws StaffDocBuilderException {
		log.info("Opening template file");

		try {
			this.template = new XWPFDocument(getClass().getClassLoader().getResourceAsStream("templates/template.docx"));
		} catch (IOException e) {
			throw new StaffDocBuilderException("Error occurred while opening template file", e);
		}

		this.educationDataBuilder = new EducationDataBuilder(template);
		this.teacherDataBuilder = new TeacherDataBuilder(template);
		this.summaryDataBuilder = new SummaryDataBuilder(template);

		return this;
	}


	public StaffDocBuilder setEducationData(final EducationData educationData) {
		log.info("Setting education data for staff doc generation");
		educationDataBuilder.setEducationData(educationData);
		return this;
	}


	public StaffDocBuilder setTeacherData(final List<TeacherRow> teachers) {
		log.info("Setting teachers data for staff doc generation");
		teacherDataBuilder.setTeachersData(teachers);
		return this;
	}



	public StaffDocBuilder setSummaryData(final SummaryData summaryData) {
		log.info("Setting summary data for staff doc generation");
		summaryDataBuilder.setSummaryData(summaryData);
		return this;
	}


	public void writeToStream(final OutputStream stream) throws StaffDocBuilderException {
		log.info("Performing staff document generation");

		try {
			educationDataBuilder.build();
			teacherDataBuilder.build();
			summaryDataBuilder.build();

			log.info("Sending generated document");
			template.write(stream);
		} catch (InsufficientDocDataException e) {
			log.warn("Failed to generate staff document", e);
			throw e;
		} catch (Exception e) {
			log.warn("Failed to generate staff document", e);
			throw new StaffDocBuilderException("Some error occurred during staff document generation", e);
		} finally {

			try {
				template.close();
			} catch (IOException e) {
				log.warn("Failed to close template!", e);
			}

		}

	}


}
