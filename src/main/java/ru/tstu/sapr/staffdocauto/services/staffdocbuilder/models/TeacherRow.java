package ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherRow {


    private String id;
    private String teacherName;
    private String employmentType;
    private String academicRank;
    private List<String> subjects;
    private String education;
    private String additionalEducation;
    private List<Double> shares;


}
