package ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EducationData {

    // edu program
    private String programName;

    private String directionCode;

    // edu direction
    private String directionName;

    private String year;

    // очная / заочная
    private EduScheduleType eduScheduleType;


}
