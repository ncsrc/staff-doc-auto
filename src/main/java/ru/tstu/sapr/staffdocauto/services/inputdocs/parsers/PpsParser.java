package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.IncorrectXlsFileException;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@Slf4j
public class PpsParser {


	private static final int CELL_TEACHER_NAME = 1;
	private static final int CELL_EMPLOYMENT_TYPE = 2;
	private static final int CELL_ACADEMIC_RANK = 3;
	private static final int CELL_SUBJECTS = 4;
	private static final int CELL_EDUCATION = 5;
	private static final int CELL_ADDITIONAL_EDUCATION = 6;


	public List<PpsData> parse(final MultipartFile file) throws XlsParserException {
		try (final HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream())) {
			return parsePps(workbook);
		} catch (OfficeXmlFileException e) {
			log.error("Incorrect PPS file format, should be xls");
			throw new IncorrectXlsFileException("Некорректный формат файла, файл должен быть формата xls.", e);
		} catch (IOException e) {
			log.error("Error while reading PPS file");
			throw new XlsParserException("Ошибка чтения файла", e);
		}
	}


	private int findStartRow(final HSSFSheet sheet) throws IncorrectXlsFileException {
		final Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			final Row row = rowIterator.next();
			final Cell cell = row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

			final String cellValue = cell.getStringCellValue().toLowerCase();

			if (cellValue.contains("фио")) {
				return row.getRowNum() + 1;
			}
		}

		throw new IncorrectXlsFileException("Некорректная справка о ППС");
	}


	private List<PpsData> parsePps(final HSSFWorkbook workbook) throws XlsParserException {
		final HSSFSheet sheet = workbook.getSheetAt(0);
		final List<PpsData> ppsData = new ArrayList<>();
		int rowNumber = findStartRow(sheet);

		try {

			while (true) {

				final HSSFRow row = sheet.getRow(rowNumber);

				if (isEndOfFile(row)) {
					break;
				}

				ppsData.add(parseRow(row));

				rowNumber++;
			}

			return ppsData;

		} catch (Exception e) {

			throw new XlsParserException("Some error during exceptions parsing has occurred ", e);

		}

	}


	private PpsData parseRow(final HSSFRow row) {
		return new PpsData(
				row.getCell(CELL_TEACHER_NAME, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue(),
				row.getCell(CELL_EMPLOYMENT_TYPE, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue(),
				row.getCell(CELL_ACADEMIC_RANK, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue(),
				row.getCell(CELL_SUBJECTS, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue(),
				row.getCell(CELL_EDUCATION, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue(),
				row.getCell(CELL_ADDITIONAL_EDUCATION, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue()
		);
	}


	private boolean isEndOfFile(final HSSFRow row) {
		if (row == null) {
			return true;
		}

		if (row.getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) == null) {
			return true;
		}

		return false;
	}

}
