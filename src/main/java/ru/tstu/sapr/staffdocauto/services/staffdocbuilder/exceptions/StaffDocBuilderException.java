package ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions;


public class StaffDocBuilderException extends Exception {

    public StaffDocBuilderException() {
        super();
    }

    public StaffDocBuilderException(final String message) {
        super(message);
    }

    public StaffDocBuilderException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

