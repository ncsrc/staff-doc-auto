package ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions;


public class InsufficientDocDataException extends StaffDocBuilderException {

	public InsufficientDocDataException() {
		super();
	}

	public InsufficientDocDataException(String message) {
		super(message);
	}

	public InsufficientDocDataException(String message, Throwable cause) {
		super(message, cause);
	}
}
