package ru.tstu.sapr.staffdocauto.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.dao.StudentSetDao;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.models.PpsFile;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.PpsService;
import ru.tstu.sapr.staffdocauto.services.inputdocs.XlsService;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.StaffDocBuilder;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.StaffDocBuilderException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.SummaryData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;
import ru.tstu.sapr.staffdocauto.ui.models.Task;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRow;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRequest;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.EasyUiResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
@Slf4j
public class TaskService {

    @Autowired
    StudentSetDao studentSetDao;

    @Autowired
    PpsService ppsService;

    @Autowired
    XlsService xlsService;

    @Autowired
    StaffDocBuilder staffDocBuilder;


    public EasyUiResponse getTasks(final DatagridRequest datagridRequest) {
        final List<DatagridRow> rows = studentSetDao.getStudentSetsForUi(datagridRequest);
        final int total = studentSetDao.countAll();

        return new EasyUiResponse<>(rows, total);
    }


    // TODO transactional ?
    public void executeTask(final Task task) throws XlsParserException {
        log.info("Starting to execute task");

        try {
            final List<XlsFileData> xlsData = xlsService.parseXlsFile(task.getXls());

            if (task.getPps() == null) {
                insertStudentSetUsingExistingPps(task, xlsData);
            } else {
                insertStudentSetUsingUploadedFile(task, xlsData);
            }

        } catch (XlsParserException e) {
            log.error("Error occurred while executing new task", e);
            throw e;
        }

    }


    public void deleteTask(final int taskId) {
        log.info("Removing task #{}", taskId);
        studentSetDao.deleteById(taskId);
    }


    public void sendDocument(final int studentSetId, final HttpServletResponse response) throws StaffDocBuilderException, IOException {
        log.info("Preparing for staff document generation");

        final EducationData educationData = studentSetDao.getEducationDataByStudentSetId(studentSetId);
        final List<TeacherRow> teacherData = ppsService.getTeachersDataForDocBuilder(studentSetId);

        setHeaders(studentSetId, response, educationData);

        staffDocBuilder.openTemplateFile()
                .setEducationData(educationData)
                .setTeacherData(teacherData)
                .setSummaryData(getSummaryData(studentSetId, teacherData))
                .writeToStream(response.getOutputStream());
    }


    public List<PpsFile> getPpsFiles() {
        return ppsService.getFiles();
    }


    private void insertStudentSetUsingUploadedFile(final Task task, final List<XlsFileData> xlsData) throws XlsParserException {
        final List<PpsData> ppsData = ppsService.parsePpsFile(task.getPps());
        final int studentSetId = studentSetDao.insert(task);
        saveXlsData(xlsData, studentSetId);
        savePpsData(task.getPps(), ppsData, studentSetId);
    }


    private void insertStudentSetUsingExistingPps(final Task task, final List<XlsFileData> xlsData) {
        final int studentSetId = studentSetDao.insert(task);
        saveXlsData(xlsData, studentSetId);
        savePpsData(task.getId(), studentSetId);
    }


    private void savePpsData(final MultipartFile pps, final List<PpsData> ppsData, final int studentSetId) {
        log.info("Saving parsed PPS data");
        final int ppsId = ppsService.saveFile(studentSetId, pps.getOriginalFilename());
        ppsService.savePpsFileContent(ppsData, ppsId);
    }


    private void savePpsData(final int ppsId, final int studentSetId) {
        log.info("Using existing PPS file #{}", ppsId);
        ppsService.linkStudentSetWithExistingPps(ppsId, studentSetId);
    }


    private void saveXlsData(final List<XlsFileData> xlsData, final int studentSetId) {
        log.info("Saving parsed XLS data");
        final int xlsId = xlsService.saveFile(studentSetId);
        xlsService.saveXlsData(xlsData, xlsId);
    }


    private void setHeaders(final int studentSetId, final HttpServletResponse response, final EducationData educationData) throws UnsupportedEncodingException {
        response.setHeader("Content-Type", "application/vnd.ms-word");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + getEncodedFilename(studentSetId, educationData) + ".docx\"");
    }


    private String getEncodedFilename(final int studentSetId, final EducationData educationData) throws UnsupportedEncodingException {
        return URLEncoder.encode(
                studentSetId + "_" + educationData.getYear() + "_"
                        + educationData.getProgramName()
                        .replaceAll(" ", "_")
                        .replaceAll(",", ""),
                "UTF-8"
        );
    }


    private SummaryData getSummaryData(final int studentSetId, final List<TeacherRow> teacherData) {
        final Map<String, String> pps = getPpsSummaryData(studentSetId);

        return new SummaryData(
                teacherData.size(),
                getTotalShares(teacherData),
                pps.get("full_year"),
                pps.get("pps_years")
        );
    }


    private double getTotalShares(final List<TeacherRow> teacherData) {
        final double totalShares = teacherData.stream()
                .flatMap((teacherRow -> teacherRow.getShares().stream()))
                .mapToDouble(share -> share)
                .sum();

        final DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.HALF_EVEN);
        return Double.valueOf(decimalFormat.format(totalShares));
    }


    private Map<String, String> getPpsSummaryData(final int studentSetId) {
        final Map<String, String> pps = new HashMap<>();
        final String filename = ppsService.getFilename(studentSetId);

        final String fullYear = getFullYear(filename);
        pps.put("full_year", fullYear);
        pps.put("pps_years", getYears(fullYear));

        return pps;
    }


    private String getYears(final String fullYear) {
        final String firstYear = getFirstYear(fullYear);
        final String secondYear = String.valueOf(Integer.valueOf(firstYear) + 1);
        return firstYear + "/" + secondYear;
    }


    private String getFirstYear(final String fullYear) {
        final Matcher matcher = Pattern.compile("\\d{4}").matcher(fullYear);
        matcher.find();
        return matcher.group();
    }


    private String getFullYear(final String filename) {
        final Matcher matcher = Pattern.compile("\\d*\\.\\d*\\.\\d*").matcher(filename);
        matcher.find();
        return matcher.group();
    }


}
