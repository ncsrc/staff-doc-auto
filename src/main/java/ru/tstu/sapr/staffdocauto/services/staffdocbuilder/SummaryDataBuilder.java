package ru.tstu.sapr.staffdocauto.services.staffdocbuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.InsufficientDocDataException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.SummaryData;


@Slf4j
class SummaryDataBuilder {

    private static final String TEMPLATE_TOTAL_TEACHERS = "total_teachers";
    private static final String TEMPLATE_TOTAL_SHARES = "total_shares";
    private static final String TEMPLATE_PPS_FULL_YEAR = "pps_full_year";
    private static final String TEMPLATE_PPS_YEARS = "pps_years";

    private XWPFDocument template;

    private SummaryData summaryData;


    SummaryDataBuilder(final XWPFDocument template) {
        this.template = template;
    }


    public void setSummaryData(final SummaryData summaryData) {
        this.summaryData = summaryData;
    }


    public void build() throws InsufficientDocDataException {
        if (summaryData == null) {
            throw new InsufficientDocDataException("Summary data is required for building document.");
        }

        mainLoop:
        for (final XWPFParagraph paragraph : template.getParagraphs()) {

            for (final XWPFRun run : paragraph.getRuns()) {
                String text = run.getText(0);

                log.debug("Got text: {}", text);

                // insert total teachers
                if (contains(text, TEMPLATE_TOTAL_TEACHERS)) {
                    replaceText(run, text, TEMPLATE_TOTAL_TEACHERS, String.valueOf(summaryData.getTotalTeachers()));
                }

                // insert total shares
                if (contains(text, TEMPLATE_TOTAL_SHARES)) {
                    replaceText(run, text, TEMPLATE_TOTAL_SHARES, String.valueOf(summaryData.getTotalShares()));
                }

                // insert pps full year
                if (contains(text, TEMPLATE_PPS_FULL_YEAR)) {
                    replaceText(run, text, TEMPLATE_PPS_FULL_YEAR, summaryData.getPpsFullYear());
                }

                // insert pps years
                if (contains(text, TEMPLATE_PPS_YEARS)) {
                    replaceText(run, text, TEMPLATE_PPS_YEARS, summaryData.getPpsYears());
                    break mainLoop;
                }

            }

        }

    }


    private void replaceText(final XWPFRun run, final String text, final String templateValue, final String data) {
        final String replacedText = text.replace(templateValue, data);
        run.setText(replacedText, 0);
    }


    private boolean contains(final String text, final String templateValue) {
        return text != null && text.contains(templateValue);
    }


}
