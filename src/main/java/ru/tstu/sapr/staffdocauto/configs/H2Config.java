package ru.tstu.sapr.staffdocauto.configs;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;


@Configuration
public class H2Config {


	@Bean(name = "h2")
	public Server h2TcpServer() throws SQLException {
		return Server.createTcpServer("-tcp").start();
	}


}
