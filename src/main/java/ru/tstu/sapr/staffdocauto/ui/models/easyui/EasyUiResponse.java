package ru.tstu.sapr.staffdocauto.ui.models.easyui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EasyUiResponse <T> {

	private T rows;
	private int total;

}
