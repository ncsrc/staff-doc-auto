package ru.tstu.sapr.staffdocauto.ui.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UiResponse {

    private String status;
    private String message;

}
