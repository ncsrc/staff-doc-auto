package ru.tstu.sapr.staffdocauto.ui.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

	private int year;
	private EduScheduleType eduScheduleType;
	private int eduProgramId;

	private MultipartFile xls;
	private MultipartFile pps;

	private int id;	// pps id, if already existing file specified


}
