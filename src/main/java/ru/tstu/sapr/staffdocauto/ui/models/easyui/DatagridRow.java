package ru.tstu.sapr.staffdocauto.ui.models.easyui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatagridRow {

	private int id;		// student set id
	private int year;
	private String directionName;
	private String programName;
	private EduQualificationType eduQualificationType;
	private EduScheduleType eduScheduleType;


}
