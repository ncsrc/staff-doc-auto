package ru.tstu.sapr.staffdocauto.ui.models.easyui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatagridRequest {

	private int page;
	private int rows;
	private String sort;
	private String order;


}
