package ru.tstu.sapr.staffdocauto.ui.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tstu.sapr.staffdocauto.models.PpsFile;
import ru.tstu.sapr.staffdocauto.models.edu.EduDirections;
import ru.tstu.sapr.staffdocauto.models.edu.EduPrograms;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;
import ru.tstu.sapr.staffdocauto.services.EducationService;
import ru.tstu.sapr.staffdocauto.services.TaskService;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.IncorrectXlsFileException;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.XlsParserException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.StaffDocBuilderException;
import ru.tstu.sapr.staffdocauto.ui.exceptions.ValidationException;
import ru.tstu.sapr.staffdocauto.ui.models.Task;
import ru.tstu.sapr.staffdocauto.ui.models.UiResponse;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRequest;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.EasyUiResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@RestController
@Slf4j
public class IndexController {

	private static final String PPS_REGEX = "^\\D*\\d{2}\\.\\d{2}\\.\\d{4}\\.\\D*$";

	private final TaskService taskService;

	@Autowired
	EducationService educationService;


	@Autowired
	public IndexController(TaskService taskService) {
		this.taskService = taskService;
	}


	@GetMapping("/task")
	public EasyUiResponse getTasks(final DatagridRequest datagridRequest) {
		return taskService.getTasks(datagridRequest);
	}


	@PostMapping("/task")
	public UiResponse setTask(final Task task) {
		log.info("----- GOT TASK: {}", task); // debug

		log.info("Got request for new task: year - {}, program #{}", task.getYear(), task.getEduProgramId());

		try {
			validateFiles(task);
		} catch (ValidationException e) {
			log.warn("Validation error: ", e);
			return sendErrorResponse(e);
		}

		try {
			taskService.executeTask(task);
		} catch (IncorrectXlsFileException e) {
			log.warn("Input xls file is incorrect", e);
			return sendErrorResponse(e);
		} catch (XlsParserException e) {
			log.warn("Error while parsing xls has occurred", e);
			return sendErrorResponse(e);
		}
		// TODO catch Exception

		return new UiResponse("OK", "");
	}


	@DeleteMapping("/task/{taskId}")
	public void deleteTask(@PathVariable("taskId") final int taskId) {
		log.info("Removing task #{}", taskId);
		taskService.deleteTask(taskId);
	}


	@GetMapping(value = "/task/{id}")
	public UiResponse getStaffDocument(@PathVariable("id") final int id, final HttpServletResponse response) throws IOException {

		// TODO check from gui error scenario

		log.info("Got request for staff document generation");

		try {
			taskService.sendDocument(id, response);
		} catch (StaffDocBuilderException e) {
			log.warn("Error while staff doc generation has occurred", e);
			return sendErrorResponse(e);
		}

		// In success case there is no need to send UiResponse
		return null;
	}


	// Combined with qualification type determines edu programs
	@GetMapping("/edu_directions")
	public List<EduDirections> getEduDirections(@RequestParam("qualification") final EduQualificationType qualification) {
		return educationService.getEduDirections(qualification);
	}


	@GetMapping("/edu_programs")
	public List<EduPrograms> getEduPrograms(@RequestParam("direction_code") final String directionCode) {
		return educationService.getEduPrograms(directionCode);
	}


	@GetMapping("/pps_files")
	public List<PpsFile> getPpsFiles() {
		return taskService.getPpsFiles();
	}


	private UiResponse sendErrorResponse(final Exception e) {
		return new UiResponse("ERROR", e.getMessage());
	}


	private void validateFiles(final Task task) throws ValidationException {
		log.info("Starting validation on new task");
		validateXls(task);
		validatePps(task);
	}


	private void validateXls(final Task task) throws ValidationException {
		if (!task.getXls().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
			throw new ValidationException("Загруженны XLS файл должен быть формата xlsX.");
		}
	}


	private void validatePps(final Task task) throws ValidationException {
		if (task.getPps() != null && !task.getPps().getContentType().equals("application/vnd.ms-excel")) {
			throw new ValidationException("Загруженны PPS файл должен быть формата xls.");
		}

		if (task.getPps() != null && !task.getPps().getOriginalFilename().matches(PPS_REGEX)) {
			throw new ValidationException("Название файла ППС должно быть следующего вида: 'pps[дата]', например: pps08.12.2017");
		}
	}



}
