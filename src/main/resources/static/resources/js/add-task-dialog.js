$(function () {

    // form input elements
    const taskForm = $('#dialog-add-task-form'),
        year = $('#task-year'),
        eduDirection = $('#task-edu-direction'),
        scheduleType = $('#task-schedule-type'),
        qualificaion = $('#task-qualification'),
        eduProgram = $('#task-edu-program'),
        fileXls = $('#task-file-xls'),
        filePps = $('#task-file-pps'),
        ppsExisting = $('#task-file-pps-existing');


    const initForm = (function () {


        function initYearTextbox() {
            year.textbox({
                label: 'Год выпуска',
                labelPosition: 'top',
                required: true,
                width: 300
            });
        }

        function initScheduleCombobox() {
            scheduleType.combobox({
                label: 'Форма обучения',
                labelPosition: 'top',
                required: true,
                valueField: 'scheduleType',
                textField: 'scheduleName',
                data: [
                    {
                        scheduleType: 'FULLTIME',
                        scheduleName: 'Очная'
                    },
                    {
                        scheduleType: 'EXTRAMURAL',
                        scheduleName: 'Заочная'
                    }
                ],
                width: 300
            });
        }


        function initQualificationCombobox() {

            qualificaion.combobox({
                label: 'Квалификация',
                labelPosition: 'top',
                required: true,
                valueField: 'qualificationType',
                textField: 'qualificationName',
                data: [
                    {
                        qualificationType: 'BACHELOR',
                        qualificationName: 'Бакалавр'
                    },
                    {
                        qualificationType: 'MASTER',
                        qualificationName: 'Магистр'
                    }
                ],
                width: 300,

                onSelect: (selected) => {
                    const qualification = selected.qualificationType;
                    const eduDirectionUrl = '/edu_directions?qualification=' + qualification;

                    eduDirection.combobox('reload', eduDirectionUrl);
                    eduProgram.combobox('loadData', ['']);
                },

                onChange: () => {
                    eduDirection.combobox('clear');
                    eduProgram.combobox('clear');
                }
            });

        }


        function initEduDirectionsCombobox() {
            eduDirection.combobox({
                label: 'Направление',
                labelPosition: 'top',
                required: true,
                valueField: 'code',
                textField: 'directionName',
                method: 'GET',
                width: 300,

                onSelect: (selected) => {
                    const direction = selected.code;
                    const eduProgramUrl = '/edu_programs?direction_code=' + direction;

                    eduProgram.combobox('reload', eduProgramUrl);
                },

                onChange: () => {
                    eduProgram.combobox('clear');
                }
            });
        }


        function initEduProgramCombobox() {
            eduProgram.combobox({
                label: 'Образовательная программа',
                labelPosition: 'top',
                required: true,
                method: 'GET',
                valueField: 'id',
                textField: 'programName',
                width: 300
            });
        }


        function initXlsFilebox() {
            fileXls.filebox({
                label: 'XLS файл',
                labelPosition: 'top',
                required: true,
                buttonText: 'Выбрать файл',
                width: 300
            });
        }


        function initPpsFilebox() {
            filePps.filebox({
                label: 'PPS файл',
                labelPosition: 'top',
                required: false,
                buttonText: 'Выбрать файл',
                width: 300,
                disabled: false,

                onChange: (newValue, oldValue) => {

                    if (newValue !== '') {
                        ppsExisting.combobox('disable');
                    }

                }
            });
        }


        function initPpsCombobox() {
            ppsExisting.combobox({
                label: 'Выбрать ППС из загруженных ранее',
                labelPosition: 'top',
                required: false,
                method: 'GET',
                url: '/pps_files',
                valueField: 'id',
                textField: 'filename',
                width: 300,
                disabled: false,

                onChange: (newValue, oldValue) => {

                    if (newValue !== '') {
                        filePps.filebox('disable');
                    }

                }
            });
        }


        function initForm() {
            initYearTextbox();

            initScheduleCombobox();
            initQualificationCombobox();
            initEduDirectionsCombobox();
            initEduProgramCombobox();

            initXlsFilebox();
            initPpsFilebox();
            initPpsCombobox();
        }


        return initForm;
    })();



    const validateForm = (function () {


        function showError(text) {
            $.messager.alert('Ошибка', text);
        }


        function validateForm() {
            if (!year.textbox('isValid')) {
                showError('Поле "Год выпуска" не заполнено');
                return false;
            }

            if (!scheduleType.combobox('isValid')) {
                showError('Поле "Форма обучения" не заполнено');
                return false;
            }

            if (!qualificaion.combobox('isValid')) {
                showError('Поле "Квалификация" не заполнено');
                return false;
            }

            if (!eduDirection.combobox('isValid')) {
                showError('Поле "Направление" не заполнено');
                return false;
            }

            if (!eduProgram.combobox('isValid')) {
                showError('Поле "Образовательная программа" не заполнено');
                return false;
            }

            if (!fileXls.combobox('isValid')) {
                showError('XSL файл не указан');
                return false;
            }


            if (filePps.filebox('getValue') === '' && ppsExisting.combobox('getValue') === '') {
                showError('Не указан PPS файл');
                return false;
            }

            return true;
        }


        return validateForm;
    })();


    // Submit button handler
    $('#task-submit').on('click', function (event) {

        const isValid = validateForm();


        if (isValid) {

            taskForm.form('submit', {
                url: '/task',
                iframe: false,

                onSubmit: (data) => {
                    data.eduProgramId = eduProgram.combobox('getValue');
                },

                onProgress: () => {
                    // TODO progressbar
                },

                success: (response) => {
                    const parsedResponse = JSON.parse(response);

                    if (parsedResponse.status === 'ERROR') {
                        console.log('ERROR: ' + parsedResponse.message);
                        $.messager.alert('Ошибка', parsedResponse.message);
                    } else {
                        $dialog.close();
                        App.datagrid.reload();
                    }

                }

            });

        }



    });


    const $dialog = $('#dialog-add-task');
    $dialog.open = () => {

        initForm();

        $dialog.dialog({
            title: 'Добавить новый набор',
            modal: true,
            resizable: true,
            width: 500
        });

        $dialog.dialog('open');
        $dialog.dialog('center');
    };

    $dialog.close = () => $dialog.dialog('close');


    // Export
    App.dialog = $dialog;

});





