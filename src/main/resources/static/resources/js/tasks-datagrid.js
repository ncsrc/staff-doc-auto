$(function () {

    // DOM elements
    const $tasksdatagrid = $('#tasks-datagrid');
    $tasksdatagrid.reload = () => $tasksdatagrid.datagrid('reload');

    const $tasksdatagridContainer = $('#tasks-datagrid-container');



    // build doc action handler
    $(document).on('click', '.datagrid-action-build-doc', function (event) {

        const id = $(this).data('id');
        console.log('id=' + id);
        location.href = '/task/' + id;

    });


    // delete task action handler
    $(document).on('click', '.datagrid-action-delete', function () {

        $.messager.confirm('Подтверждение', 'Вы уверены, что хотите удалить данный набор студентов?', (approved) => {

            if (approved) {
                const id = $(this).data('id');

                $.ajax({
                    url: '/task/' + id,
                    method: 'DELETE',

                    success: function () {
                        $tasksdatagrid.reload();
                    }
                });
            }

        });


    });


    const toolbar = [{
        text: 'Добавить',
        iconCls: 'icon-add-task',

        handler: () => {
            App.dialog.open();
        }
    }];


    const formatters = {

        actionsFormatter(value, row, index) {
            const id = row.id;

            return '<div class="tasks-datagrid-actions">' +
                '<a class="datagrid-action-build-doc easyui-linkbutton" data-id="' + id + '" data-options="iconCls: \'icon-word\', plain: true, size: \'large\'"></a>' +
                '<a class="datagrid-action-delete easyui-linkbutton" data-id="' + id + '" data-options="iconCls: \'icon-remove-task\', plain: true, size: \'large\'"></a>' +
                '</div>'
        },

        eduScheduleTypeFormatter(value, row, index) {
            if (value === 'FULLTIME') {
                return 'Очная';
            } else if (value === 'EXTRAMURAL') {
                return 'Заочная';
            } else {
                return value;
            }
        },

        eduQualificationTypeFormatter(value, row, index) {
            if (value === 'BACHELOR') {
                return 'Бакалавр';
            } else if (value === 'MASTER') {
                return 'Магистр';
            } else {
                return value;
            }

        }

    };


    $tasksdatagrid.datagrid({
        url: '/task',
        method: 'GET',
        fit: true,
        singleSelect: true,
        fitColumns: true,
        toolbar: toolbar,
        striped: true,
        pagination: true,
        sortName: 'id',
        sortOrder: 'desc',
        border: '',
        nowrap: false,


        columns: [[
            {
                field: 'id',    // TODO set hidden, enable rownumbers
                title: 'ID',
                width: 5,
                sortable: true,
                order: 'desc',
                resizable: true,
                align: 'center'
            },
            {
                field: 'year',
                title: 'Год выпуска',
                width: 15,
                sortable: true,
                order: 'desc',
                resizable: true,
                align: 'center'
            },
            {
                field: 'directionName',
                title: 'Направление',
                width: 30,
                // sortable: true,
                // order: 'desc',
                resizable: true,
                align: 'center'
            },
            {
                field: 'programName',
                title: 'Образовательная программа',
                width: 40,
                // sortable: true,
                // order: 'desc',
                resizable: true,
                align: 'center'
            },
            {
                field: 'eduQualificationType',
                title: 'Квалификация',
                width: 15,
                // sortable: true,
                // order: 'desc',
                resizable: true,
                align: 'center',
                formatter: formatters.eduQualificationTypeFormatter
            },
            {
                field: 'eduScheduleType',
                title: 'Форма обучения',
                width: 15,
                // sortable: true,
                // order: 'desc',
                resizable: true,
                align: 'center',
                formatter: formatters.eduScheduleTypeFormatter
            },
            {
                field: 'actions',
                title: 'Действия',
                width: 20,
                resizable: true,
                align: 'center',
                formatter: formatters.actionsFormatter
            }
        ]],


        onLoadSuccess: (data) => {

            // initialize easyui components
            $tasksdatagridContainer.find('.easyui-linkbutton').linkbutton();
        }


    });


    // Export
    App.datagrid = $tasksdatagrid;


});







