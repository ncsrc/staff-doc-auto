package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.tstu.sapr.staffdocauto.models.PpsFile;
import ru.tstu.sapr.staffdocauto.models.PpsFileStudentSet;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class PpsFileDaoTest {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private PpsFileDao dao;

	private static final List<PpsFile> INITIAL_PPS_FILES_DATA = Arrays.asList(
			new PpsFile(1, "pps-file-1"),
			new PpsFile(2, "pps-file-2")
	);

	private static final List<PpsFileStudentSet> INITIAL_PPS_STUDENTSETS_DATA = Arrays.asList(
			new PpsFileStudentSet(1, 1),
			new PpsFileStudentSet(2, 2)
	);



	@Before
	public void setUp() throws Exception {
		dao = new PpsFileDao(namedParameterJdbcTemplate, jdbcTemplate);
	}


	@Test
	public void getAll_returns_correct_data() throws Exception {
		final List<PpsFile> actual = dao.getAllFiles();
		assertThat(actual).isEqualTo(INITIAL_PPS_FILES_DATA);
	}


	@Test
	public void after_insert_data_changed() throws Exception {
		final String newFileName = "pps-file-3";

		final int id = dao.insertFile(newFileName);
		final List<PpsFile> actual = dao.getAllFiles();

		assertThat(actual.size()).isEqualTo(3);
		assertThat(id).isEqualTo(3);
	}


	@Test
	public void after_insert_file_studentset_data_changed() throws Exception {
		// need additional student set
	}


	@Test
	public void getFilenameByStudentSetId_returns_correct_data() throws Exception {
		final String actual = dao.getFilenameByStudentSetId(1);
		assertThat(actual).isEqualTo(INITIAL_PPS_FILES_DATA.get(0).getFilename());
	}


	@Test
	public void getAllFiles_returns_correct_data() throws Exception {
		final List<PpsFile> actual = dao.getAllFiles();
		assertThat(actual).isEqualTo(INITIAL_PPS_FILES_DATA);
	}


	@Test
	public void insertFileStudentSetRelation() throws Exception {

	}


}