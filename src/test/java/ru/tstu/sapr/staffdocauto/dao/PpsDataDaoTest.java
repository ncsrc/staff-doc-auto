package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.tstu.sapr.staffdocauto.services.inputdocs.models.Teacher;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class PpsDataDaoTest {


	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	private PpsDataDao dao;


	@Before
	public void setUp() throws Exception {
		dao = new PpsDataDao(jdbcTemplate);
	}


	@Test
	public void getTeachersByStudentSetId_returns_correct_result() throws Exception {
		final List<Teacher> expected = getTeachers();
		final List<Teacher> actual = dao.getTeachersByStudentSetId(1);
		assertThat(actual).isEqualTo(expected);
	}



	private List<Teacher> getTeachers() {
		return Arrays.asList(
				new Teacher(
						"firstname-1 lastname-1", "empl-1", "rank-1",
						"subject-1", "edu-1", "add-edu-1", 0.05
				),

				new Teacher(
						"firstname-1 lastname-1", "empl-1", "rank-1",
						"subject-2", "edu-1", "add-edu-1", 0.06
				),

				new Teacher(
						"firstname-2 lastname-2", "empl-2", "rank-2",
						"subject-3", "edu-2", "add-edu-2", 0.15
				),

				new Teacher(
						"firstname-2 lastname-2", "empl-2", "rank-2",
						"subject-4", "edu-2", "add-edu-2", 0.16
				)

		);
	}

}