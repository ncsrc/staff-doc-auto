package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.tstu.sapr.staffdocauto.models.StudentSet;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;
import ru.tstu.sapr.staffdocauto.ui.models.Task;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRequest;
import ru.tstu.sapr.staffdocauto.ui.models.easyui.DatagridRow;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class StudentSetDaoTest {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private StudentSetDao dao;

	private static final List<StudentSet> INITIAL_DATA = Arrays.asList(
			new StudentSet(1, 1, EduScheduleType.FULLTIME, 2017),
			new StudentSet(2, 2, EduScheduleType.FULLTIME, 2018)
	);



	@Before
	public void setUp() throws Exception {
		dao = new StudentSetDao(namedParameterJdbcTemplate, jdbcTemplate);
	}


	@Test
	public void getStudentSetsForUi_returns_correct_result() throws Exception {
		final List<DatagridRow> expected = Arrays.asList(
				new DatagridRow(
						1, 2017, "some-direction",
						"some-program-1", EduQualificationType.BACHELOR,
						EduScheduleType.FULLTIME
				)
		);

		final List<DatagridRow> actual = dao.getStudentSetsForUi(new DatagridRequest(0, 1, "id", "asc"));

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void countAll_returns_correct_result() throws Exception {
		final int actual = dao.countAll();
		assertThat(actual).isEqualTo(INITIAL_DATA.size());
	}


	@Test
	public void getEducationDataByStudentSetId_returns_correct_result() throws Exception {
		final EducationData expected = new EducationData(
				"some-program-1", "38.08.03",
				"some-direction", "2017", EduScheduleType.FULLTIME
		);

		final EducationData actual = dao.getEducationDataByStudentSetId(1);
		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void after_insert_count_changed() throws Exception {
		final int id = dao.insert(new Task(2015, EduScheduleType.EXTRAMURAL, 1, null, null, 1));
		final List<DatagridRow> studentSets = dao.getStudentSetsForUi(new DatagridRequest(0, 10, "id", "asc"));
		final int actual = dao.countAll();
		assertThat(actual).isEqualTo(INITIAL_DATA.size() + 1);
		assertThat(id).isEqualTo(3);
	}


	@Test
	public void after_deleteById_count_changed() throws Exception {
		dao.deleteById(1);
		final int actual = dao.countAll();
		assertThat(actual).isEqualTo(INITIAL_DATA.size() - 1);
	}

}