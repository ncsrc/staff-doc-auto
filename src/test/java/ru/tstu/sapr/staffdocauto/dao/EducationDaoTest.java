package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.tstu.sapr.staffdocauto.models.edu.EduDirections;
import ru.tstu.sapr.staffdocauto.models.edu.EduPrograms;
import ru.tstu.sapr.staffdocauto.models.edu.EduQualificationType;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class EducationDaoTest {


	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	private EducationDao dao;

	private static final List<EduDirections> EDU_DIRECTIONS = Arrays.asList(
			new EduDirections("38.08.03", "some-direction", EduQualificationType.BACHELOR),
			new EduDirections("38.08.04", "some-direction", EduQualificationType.MASTER)
	);

	private static final List<EduPrograms> EDU_PROGRAMS = Arrays.asList(
			new EduPrograms(1, "38.08.03", "some-program-1"),
			new EduPrograms(2, "38.08.03", "some-program-2")
	);



	@Before
	public void setUp() throws Exception {
		dao = new EducationDao();
		dao.jdbcTemplate = this.jdbcTemplate;
	}


	@Test
	public void getEduDirections_returns_correct_data() throws Exception {
		final EduQualificationType qualificationType = EduQualificationType.BACHELOR;
		final List<EduDirections> actual = dao.getEduDirectionsByQualificationType(qualificationType);
		assertThat(actual.get(0)).isEqualTo(EDU_DIRECTIONS.get(0));
	}


	@Test
	public void getEduProgramsByDirectionCode_returns_correct_data() throws Exception {
		final List<EduPrograms> actual = dao.getEduProgramsByDirectionCode("38.08.03");
		assertThat(actual).isEqualTo(EDU_PROGRAMS);
	}

}