package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class XlsFileDataDaoTest {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	private XlsFileDataDao dao;

	private static final List<XlsFileData> INITIAL_DATA = Arrays.asList(
			new XlsFileData(1, "subject-1", "lastname-1", 0.05),
			new XlsFileData(1, "subject-2", "lastname-1", 0.06),

			new XlsFileData(1, "subject-3", "lastname-2", 0.15),
			new XlsFileData(1, "subject-4", "lastname-2", 0.16)
	);


	@Before
	public void setUp() throws Exception {
		this.dao = new XlsFileDataDao(jdbcTemplate);
	}


	@Test
	public void after_insert_table_changed() throws Exception {

		dao.insert(Arrays.asList(
				new XlsFileData(1, "subject-5", "lastname-3", 0.1),
				new XlsFileData(1, "subject-6", "lastname-3", 0.2)
		));

	}


}