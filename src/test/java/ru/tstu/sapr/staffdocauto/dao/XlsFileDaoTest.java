package ru.tstu.sapr.staffdocauto.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


@RunWith(SpringJUnit4ClassRunner.class)
@JdbcTest
@TestPropertySource("classpath:/db/test-db.properties")
@ActiveProfiles("test")
public class XlsFileDaoTest {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;

    private XlsFileDao dao;


    @Before
    public void setUp() throws Exception {
        dao = new XlsFileDao(namedParameterJdbcTemplate, jdbcTemplate);
    }


    @Test
    public void after_insert_correct_id_returned() {
        final int id = dao.insert(1);
        assertThat(id).isEqualTo(3);
    }


}