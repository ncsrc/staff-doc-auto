package ru.tstu.sapr.staffdocauto.services.inputdocs;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.tstu.sapr.staffdocauto.dao.PpsDataDao;
import ru.tstu.sapr.staffdocauto.services.inputdocs.models.Teacher;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class PpsServiceTest {

	private PpsService ppsService;

	@Mock
	private PpsDataDao ppsDataDao;


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		this.ppsService = new PpsService();
		ppsService.ppsDataDao = this.ppsDataDao;
	}


	@Test
	public void getTeachersDataForDocBuilder_returns_correct_data() throws Exception {
		final int studentSetId = 1;
		given(ppsDataDao.getTeachersByStudentSetId(studentSetId)).willReturn(getTeachers());
		final List<TeacherRow> expected = getExpectedTeacherRows();

		final List<TeacherRow> actual = ppsService.getTeachersDataForDocBuilder(studentSetId);

		verify(ppsDataDao).getTeachersByStudentSetId(studentSetId);
		assertThat(actual).isEqualTo(expected);
	}


	private List<TeacherRow> getExpectedTeacherRows() {
		return Arrays.asList(
				new TeacherRow(
						"1", "а б", "empl", "rank",
						Arrays.asList("subject", "subject2"),
						"edu", "add-edu",
						Arrays.asList(0.05, 0.06)
				),

				new TeacherRow(
						"2", "б а", "empl", "rank",
						Arrays.asList("subject3", "subject4"),
						"edu", "add-edu",
						Arrays.asList(0.07, 0.08)
				)
		);
	}

	private List<Teacher> getTeachers() {
		return Arrays.asList(
				new Teacher("б а", "empl", "rank",
						"subject3", "edu", "add-edu", 0.07
				),

				new Teacher("б а", "empl", "rank",
						"subject4", "edu", "add-edu", 0.08
				),

				new Teacher("а б", "empl", "rank",
						"subject", "edu", "add-edu", 0.05
						),

				new Teacher("а б", "empl", "rank",
						"subject2", "edu", "add-edu", 0.06
				)
		);
	}

}