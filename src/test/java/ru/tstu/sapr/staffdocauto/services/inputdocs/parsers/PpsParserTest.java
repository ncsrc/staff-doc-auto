package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.PpsParser;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.IncorrectXlsFileException;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class PpsParserTest {

	private PpsParser parser;

	@Mock
	private MultipartFile file;


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		parser = new PpsParser();
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_not_xls_file_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("pps/test-pps-not-xls.xlsx"));
		parser.parse(file);
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_start_row_not_found_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("pps/test-pps-incorrect.xls"));
		parser.parse(file);
	}


	@Test
	public void when_parse_file_with_empty_cells_then_correct_parsed_data() throws Exception {
		final List<PpsData> expected = Arrays.asList(
				new PpsData(
						"Абакумова Нина Алексеевна", "",
						" должность – доцент", "",
						"уровень образования – высшее", ""
				),
				new PpsData(
						"asd", "штатный",
						"", "Архитектурно-строительное черчение;\nСтроительное черчение;",
						"", "повышение квалификации: «Углеродные наносистемы: получение"
				)
		);

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("pps/test-pps-with-empty-cells.xls"));

		final List<PpsData> actual = parser.parse(file);

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void when_parse_correct_file_then_correct_parsed_data() throws Exception {

		final List<PpsData> expected = Arrays.asList(
				new PpsData(
						"Абакумова Нина Алексеевна", "штатный",
						" должность – доцент", "Химия",
						"уровень образования – высшее", "повышение квалификации: «Новые материалы и технологии в науке, образовании и производстве»"
				),
				new PpsData(
						"Абоносимов Олег Аркадьевич", "штатный",
						" должность – доцент", "Архитектурно-строительное черчение;\nСтроительное черчение;",
						"уровень образования – высшее", "повышение квалификации: «Углеродные наносистемы: получение"
				)
		);

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("pps/test-pps-correct.xls"));

		final List<PpsData> actual = parser.parse(file);

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void parse_full_file() throws Exception {
		final long startTime = System.currentTimeMillis();

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("pps/test-pps-full.xls"));

		parser.parse(file);

		final long endTime = System.currentTimeMillis();

		System.out.println("SPENT TIME: " + ((endTime - startTime) / 1000));
	}


}