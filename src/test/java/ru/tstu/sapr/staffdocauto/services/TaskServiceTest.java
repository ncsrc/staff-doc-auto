package ru.tstu.sapr.staffdocauto.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.dao.StudentSetDao;
import ru.tstu.sapr.staffdocauto.models.PpsData;
import ru.tstu.sapr.staffdocauto.models.StudentSet;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;
import ru.tstu.sapr.staffdocauto.services.inputdocs.PpsService;
import ru.tstu.sapr.staffdocauto.services.inputdocs.XlsService;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.StaffDocBuilder;
import ru.tstu.sapr.staffdocauto.ui.models.Task;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class TaskServiceTest {

	@Mock
	private StudentSetDao studentSetDao;

	@Mock
	private PpsService ppsService;

	@Mock
	private XlsService xlsService;

	@Mock
	private StaffDocBuilder staffDocBuilder;

	private TaskService service;


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		service = new TaskService();
		service.studentSetDao = studentSetDao;
		service.ppsService = ppsService;
		service.xlsService = xlsService;
		service.staffDocBuilder = staffDocBuilder;
	}


	@Test
	public void executeTask() throws Exception {

		final MultipartFile ppsMock = mock(MultipartFile.class);
		given(ppsMock.getOriginalFilename()).willReturn("pps_file");
		final MultipartFile xlsMock = mock(MultipartFile.class);
		final Task taskMock = new Task(2017, EduScheduleType.FULLTIME, 1, xlsMock, ppsMock, 1);

		final ArrayList<XlsFileData> xlsFileData = new ArrayList<>();
		final ArrayList<PpsData> ppsData = new ArrayList<>();
		given(xlsService.parseXlsFile(xlsMock)).willReturn(xlsFileData);
		given(ppsService.parsePpsFile(ppsMock)).willReturn(ppsData);
		given(studentSetDao.insert(taskMock)).willReturn(1);
		given(ppsService.saveFile(1, "pps_file")).willReturn(1);
		given(xlsService.saveFile(1)).willReturn(1);

		service.executeTask(taskMock);


		// parsing of files
		verify(xlsService).parseXlsFile(xlsMock);
		verify(ppsService).parsePpsFile(ppsMock);

		// inserting new student set
		verify(studentSetDao).insert(taskMock);

		// saving xls data
		verify(xlsService).saveFile(1);
		verify(xlsService).saveXlsData(xlsFileData, 1);

		// saving pps data
		verify(ppsService).saveFile(1, "pps_file");
		verify(ppsService).savePpsFileContent(ppsData, 1);
	}


}