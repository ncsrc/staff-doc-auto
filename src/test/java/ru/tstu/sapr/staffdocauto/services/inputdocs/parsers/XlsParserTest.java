package ru.tstu.sapr.staffdocauto.services.inputdocs.parsers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.web.multipart.MultipartFile;
import ru.tstu.sapr.staffdocauto.models.XlsFileData;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.XlsParser;
import ru.tstu.sapr.staffdocauto.services.inputdocs.parsers.exceptions.IncorrectXlsFileException;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class XlsParserTest {


	private XlsParser xlsParser;

	@Mock
	private MultipartFile file;


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		xlsParser = new XlsParser();
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_not_xlsx_file_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-not-xlsx.xls"));
		xlsParser.parse(file);
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_start_row_not_found_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-no-start-row.xlsx"));
		xlsParser.parse(file);
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_teacher_column_not_found_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-no-teacher-column.xlsx"));
		xlsParser.parse(file);
	}


	@Test(expected = IncorrectXlsFileException.class)
	public void when_share_column_not_found_then_exception() throws Exception {
		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-no-share-column.xlsx"));
		xlsParser.parse(file);
	}


	@Test
	public void when_parse_file_with_empty_cells_then_correct_parsed_data() throws Exception {
		final List<XlsFileData> expected = Arrays.asList(
				new XlsFileData("Теория принятия решений", "Майстренко", 0.05),
				new XlsFileData("Организация научных исследований", "", 0),
				new XlsFileData("Вычислительные системы промышленных предприятий", "", 0)
		);

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-with-blanks.xlsx"));

		final List<XlsFileData> actual = xlsParser.parse(file);

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void when_parse_correct_file_then_correct_parsed_data() throws Exception {

		final List<XlsFileData> expected = Arrays.asList(
				new XlsFileData("Теория принятия решений", "Майстренко", 0.05),
				new XlsFileData("Организация научных исследований", "Литовка", 0.05),
				new XlsFileData("Вычислительные системы промышленных предприятий", "Лоскутов", 0.05)
		);

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-correct.xlsx"));

		final List<XlsFileData> actual = xlsParser.parse(file);

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void parse_full_file() throws Exception {

		given(file.getInputStream()).willReturn(getClass().getClassLoader().getResourceAsStream("xls/test-xls-full.xlsx"));

		final List<XlsFileData> actual = xlsParser.parse(file);

		System.out.println(actual);
	}

}