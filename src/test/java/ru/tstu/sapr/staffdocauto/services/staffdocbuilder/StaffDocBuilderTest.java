package ru.tstu.sapr.staffdocauto.services.staffdocbuilder;

import org.junit.Before;
import org.junit.Test;
import ru.tstu.sapr.staffdocauto.models.edu.EduScheduleType;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.exceptions.InsufficientDocDataException;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.EducationData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.SummaryData;
import ru.tstu.sapr.staffdocauto.services.staffdocbuilder.models.TeacherRow;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


public class StaffDocBuilderTest {

    private String RESOURCES_PATH;
    private String FILLED_TEMPLATE_PATH;

    private StaffDocBuilder docxBuilder;

    private EducationData educationData;
    private SummaryData summaryData;


    @Before
    public void setUp() throws Exception {
        this.RESOURCES_PATH = getClass().getClassLoader().getResource(".").getPath();
        this.FILLED_TEMPLATE_PATH = RESOURCES_PATH + "filled_template.docx";
        this.docxBuilder = new StaffDocBuilder();

        this.educationData = new EducationData(
                "edu", "1.1.1", "dir",
                "asdasd", EduScheduleType.FULLTIME
        );

        this.summaryData = new SummaryData(
                2, 15.5, "2017.01.02", "2017/2018"
        );
    }


    @Test
    public void when_open_file_then_not_null() throws Exception {
        docxBuilder.openTemplateFile();
        assertNotNull(docxBuilder.template);
    }


    @Test(expected = InsufficientDocDataException.class)
    public void when_setEducationData_only_then_exception() throws Exception {
        FileOutputStream outputStream = new FileOutputStream(FILLED_TEMPLATE_PATH);

        docxBuilder.openTemplateFile()
                .setEducationData(educationData)
                .writeToStream(outputStream);
    }


    @Test(expected = InsufficientDocDataException.class)
    public void when_setTeachersData_only_then_exception() throws Exception {
        FileOutputStream outputStream = new FileOutputStream(FILLED_TEMPLATE_PATH);

        docxBuilder.openTemplateFile()
                .setTeacherData(getTeachers())
                .writeToStream(outputStream);
    }


    @Test(expected = InsufficientDocDataException.class)
    public void when_setSummaryData_only_then_exception() throws Exception {
        FileOutputStream outputStream = new FileOutputStream(FILLED_TEMPLATE_PATH);

        docxBuilder.openTemplateFile()
                .setSummaryData(summaryData)
                .writeToStream(outputStream);
    }


    // !!! check file content manually
    @Test
    public void when_all_data_supplied_then_file_exists_and_no_exceptions() throws Exception {
        final FileOutputStream outputStream = new FileOutputStream(FILLED_TEMPLATE_PATH);

        docxBuilder.openTemplateFile()
                .setEducationData(educationData)
                .setTeacherData(getTeachers())
                .setSummaryData(summaryData)
                .writeToStream(outputStream);

        boolean fileExists = Files.exists(Paths.get(FILLED_TEMPLATE_PATH));

        assertTrue(fileExists);
    }


    private List<TeacherRow> getTeachers() {
        final TeacherRow teacher = new TeacherRow(
                "1", "FIO", "EMPLOYMENT_DATA", "RANK_DATA",
                Arrays.asList("subject_1", "subject_2", "subject_3"),
                "EDUCATION_DATA", "ADDITIONAL_EDU_DATA",
                Arrays.asList(11.2, 11.3, 0.3)
        );

        final TeacherRow teacher1 = new TeacherRow(
                "2", "FIOFIOFIOFIOFIO", "EMPLOYMENT_DATAEMPLOYMENT_DATA", "RANK_DATARANK_DATARANK_DATA",
                Arrays.asList("subject_1subject_1subject_1subject_1", "subject_2subject_2", "subject_3"),
                "EDUCATION_DATAEDUCATION_DATAEDUCATION_DATA", "ADDITIONAL_EDU_DATAADDITIONAL_EDU_DATA",
                Arrays.asList(11.2, 11.3, 0.3)
        );


        return Arrays.asList(teacher, teacher1);
    }

}